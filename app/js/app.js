/**
 * Created by nihel akremi on 07/07/14.
 */


'use strict';
var app = angular.module('app', ['ContactBookManager', 'ContactBookDashManager']);

var ContactBookManager = angular.module('ContactBookManager', ['ngRoute']);
var ContactBookDashManager = angular.module('ContactBookDashManager', ['ngRoute']);

ContactBookManager.factory('clientService', function ($rootScope, $http, $q, $log) {
    $rootScope.status = 'Retrieving data...';
    var deferred = $q.defer();
    $http.get('rest/queryClient')
        .success(function (data, status, headers, config) {
            $rootScope.clients = data;
            deferred.resolve();
            $rootScope.status = '';
        });
    return deferred.promise;
});

ContactBookManager.factory('myHttpInterceptor', function ($rootScope, $q) {
    return {
        'requestError': function (config) {
            $rootScope.status = 'HTTP REQUEST ERROR ' + config;
            return config || $q.when(config);
        },
        'responseError': function (rejection) {
            $rootScope.status = 'HTTP RESPONSE ERROR ' + rejection.status + '\n' +
                rejection.data;
            return $q.reject(rejection);
        }
    };
});

ContactBookManager.config(function ($routeProvider) {

    $routeProvider.when('/client', {
        controller: 'MainCtrl',
        templateUrl: '/partials/main.html',
        resolve: { 'clientService': 'clientService' }
    });

    $routeProvider.when('/invite', {
        controller: 'InsertCtrl',
        templateUrl: '/partials/insert.html'
    });
    $routeProvider.when('/update/:id', {
        controller: 'UpdateCtrl',
        templateUrl: '/partials/update.html',
        resolve: { 'clientService': 'clientService' }
    });

    $routeProvider.when('/info/:id', {
        controller: 'infoCtrl',
        templateUrl: '/partials/clientDetails.html',
        resolve: { 'clientService': 'clientService' }
    });

    $routeProvider.otherwise({
        redirectTo: '/'
    });
});

ContactBookManager.config(function ($httpProvider) {
    $httpProvider.interceptors.push('myHttpInterceptor');
});


ContactBookManager.controller('MainCtrl', function ($scope, $rootScope, $log, $http, $routeParams, $location, $route) {

    $scope.invite = function () {
        $location.path('/invite');
    };

    $scope.update = function (client) {
        $location.path('/update/' + client.id);
    };

    $scope.readClient = function (client) {
        $location.path('/info/' + client.id);
    };


    $scope.delete = function (client) {

        $rootScope.status = 'Deleting client ' + client.id + '...';
        $http.post('/rest/delete', {'id': client.id})
            .success(function (data, status, headers, config) {
                for (var i = 0; i < $rootScope.clients.length; i++) {
                    if ($rootScope.clients[i].id == client.id) {
                        $rootScope.clients.splice(i, 1);
                        break;
                    }
                }
                $rootScope.status = '';
            });

    };
     $scope.predicate = '-first_name';

});

//insert contact controller
ContactBookManager.controller('InsertCtrl', function ($scope, $rootScope, $log, $http, $routeParams, $location, $route) {


    //submit insert function
    $scope.submitInsert = function (isValid) {

        if (isValid) {
            var client = {
                first_name: $scope.first_name,
                last_name: $scope.last_name,
                email: $scope.email,
                address: $scope.address,
                company: $scope.company,
                type_client: $scope.type_client,
                category: $scope.category,
                note: $scope.note
            };

            $rootScope.status = 'Creating...';
            $http.post('/rest/insert', client)
                .success(function (data, status, headers, config) {
                    $rootScope.clients.push(data);
                    $rootScope.status = '';
                });
            $location.path('/client');

        }
    }

    // cancel submition
    $scope.cancelInsert = function () {
        $location.path('/client');
    };
});


//update contact infos controller
ContactBookManager.controller('UpdateCtrl', function ($routeParams, $rootScope, $scope, $log, $http, $location) {

    for (var i = 0; i < $rootScope.clients.length; i++) {
        if ($rootScope.clients[i].id == $routeParams.id) {
            $scope.client = angular.copy($rootScope.clients[i]);
        }
    }

    $scope.submitUpdate = function (isValid) {

        if (isValid) {
            $rootScope.status = 'Updating...';
            $http.post('/rest/update', $scope.client)
                .success(function (data, status, headers, config) {
                    for (var i = 0; i < $rootScope.clients.length; i++) {
                        if ($rootScope.clients[i].id == $scope.client.id) {
                            $rootScope.clients.splice(i, 1);
                            break;
                        }
                    }
                    $rootScope.clients.push(data);
                    $rootScope.status = '';
                });
            $location.path('/client');
        }
    };

    $scope.cancelUpdate = function () {
        $location.path('/client');
    };

});


// info contact controller
ContactBookManager.controller('infoCtrl', function ($routeParams, $rootScope, $scope, $log, $http, $location) {

    for (var i = 0; i < $rootScope.clients.length; i++) {
        if ($rootScope.clients[i].id == $routeParams.id) {
            $scope.client = angular.copy($rootScope.clients[i]);
        }
    }


    $scope.delete = function (client) {
        $rootScope.status = 'Deleting client ' + client.id + '...';
        $http.post('/rest/delete', {'id': client.id})
            .success(function (data, status, headers, config) {
                for (var i = 0; i < $rootScope.clients.length; i++) {
                    if ($rootScope.clients[i].id == client.id) {
                        $rootScope.clients.splice(i, 1);
                        break;
                    }
                }
                $rootScope.status = '';
            });
        $location.path('/client');
    };
});



/*

dashboard

 */


ContactBookDashManager.config(function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: '/partials/dashboard.html'

    });


});