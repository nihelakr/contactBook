__author__ = 'nihel akremi'

from google.appengine.ext import ndb
import datetime

# client class
class Client(ndb.Model):
    # personal infos
    first_name = ndb.StringProperty()
    last_name = ndb.StringProperty()

    # contact details
    email = ndb.StringProperty()
    address = ndb.StringProperty()


    #work details
    company = ndb.StringProperty()

    #client details
    type_client = ndb.StringProperty()
    category = ndb.StringProperty()
    note = ndb.StringProperty()

    #details for admin

    created = ndb.DateProperty(auto_now_add=True)
    last_modified = ndb.DateTimeProperty(auto_now=True)


# list all clients on the datastore
def AllClients():
    return Client.query()


# update client informations
def UpdateClient(id, first_name, last_name, email, address, company, type_client, category, note):
    client = Client(id=id, first_name=first_name, last_name=last_name, email=email, address=address,
                    company=company, type_client=type_client, category=category, note=note)
    client.put()
    return client


#add a new client
def InsertClient(first_name, last_name, email, address, company, type_client, category, note):
    client = Client(first_name=first_name, last_name=last_name, email=email, address=address,
                    company=company, type_client=type_client, category=category, note=note)
    client.put()
    return client


#delete a client
def DeleteClient(id):
    key = ndb.Key(Client, id)
    key.delete()

