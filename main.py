import json
from json import JSONEncoder

import datetime
import webapp2
import clientModel



# json file structure
def AsDict(client):
    return {'id': client.key.id(),
            'first_name': client.first_name,
            'last_name': client.last_name,
            'email': client.email,
            'address': client.address,
            'company': client.company,
            'type_client': client.type_client,
            'category': client.category,
            'note': client.note,
            'created': client.created,
            'last_modified': client.last_modified
    }


# json file communication and dispatching
# date encoder
class DateEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.date):
            return obj.isoformat()
        return JSONEncoder.default(self, obj)


class ClientRestHandler(webapp2.RequestHandler):
    def dispatch(self):
        super(ClientRestHandler, self).dispatch()

    def ClientSendJson(self, r):
        self.response.headers['content-type'] = 'text/plain'
        self.response.write(json.dumps(r, cls=DateEncoder))


# list all clients with json file
class ClientQueryHandler(ClientRestHandler):
    def get(self):
        clients = clientModel.AllClients()
        r = [AsDict(client) for client in clients]
        self.ClientSendJson(r)


# client update handler
class ClientUpdateHandler(ClientRestHandler):
    def post(self):
        r = json.loads(self.request.body)
        client = clientModel.UpdateClient(r['id'], r['first_name'], r['last_name'], r['email'], r['address'],
                                          r['company'], r['type_client'], r['category'], r['note'])
        r = AsDict(client)
        self.ClientSendJson(r)


# client insert handler
class ClientInsertHandler(ClientRestHandler):
    def post(self):
        r = json.loads(self.request.body)
        client = clientModel.InsertClient(r['first_name'], r['last_name'], r['email'], r['address'],
                                          r['company'], r['type_client'], r['category'], r['note'])
        r = AsDict(client)
        self.ClientSendJson(r)


# client delete handler
class ClientDeleteHandler(ClientRestHandler):
    def post(self):
        r = json.loads(self.request.body)
        clientModel.DeleteClient(r['id'])


APP = webapp2.WSGIApplication([

                                  # client routes
                                  ('/rest/queryClient', ClientQueryHandler),
                                  ('/rest/insert', ClientInsertHandler),
                                  ('/rest/delete', ClientDeleteHandler),
                                  ('/rest/update', ClientUpdateHandler),

                              ], debug=True)